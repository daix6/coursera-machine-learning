data = load('ex1data2.txt');
X = data(:, 1:2);
y = data(:, 3);
m = length(y);

[X mu sigma] = featureNormalize(X);
X = [ones(m, 1), X];

num_iters = 400;

alpha = 0.01;
theta = zeros(3, 1);
[theta, J_history] = gradientDescentMulti(X, y, theta, alpha, num_iters);

figure;
plot(1:numel(J_history), J_history, 'y', 'LineWidth', 2);xlabel('Number of iterations');
ylabel('Cost J');
hold on;

alpha = 0.1;
theta = zeros(3, 1);
[theta, J_history] = gradientDescentMulti(X, y, theta, alpha, num_iters);

plot(1:numel(J_history), J_history, 'r', 'LineWidth', 2);

alpha = 0.3;
theta = zeros(3, 1);
[theta, J_history] = gradientDescentMulti(X, y, theta, alpha, num_iters);

plot(1:numel(J_history), J_history, 'g', 'LineWidth', 2);

alpha = 1;
theta = zeros(3, 1);
[theta, J_history] = gradientDescentMulti(X, y, theta, alpha, num_iters);

plot(1:numel(J_history), J_history, 'b', 'LineWidth', 2);

print -dpng 'compareAlpha.png';